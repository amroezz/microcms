<?php /* /home/amr/Desktop/projects/microcms/microcms/resources/views/article.blade.php */ ?>
<?php $__env->startSection('content'); ?>
<div class="container" >
    <div class="row " >
        <div class="col-md-12">
            <div class="card" >
                <div class="card-header">
                  <div class="links">
                    <?php $__currentLoopData = $sections; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $section): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                       <a href="<?php echo e(url('section')); ?>/<?php echo e($section->id); ?>"> <?php echo e($section->section_title); ?> </a> 
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </div>
                </div>                 
                <div class="card-body" >                
                       <div class="row" >
                       <img src="<?php echo e(url($article->article_thumb)); ?>" >
                       </div>
                       <div class="row" >
                       <?php echo $article->article_details; ?>

                       </div>
                </div>                 
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>