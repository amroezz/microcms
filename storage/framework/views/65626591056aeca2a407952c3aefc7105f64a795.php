<?php /* /home/amr/Desktop/projects/microcms/microcms/resources/views/section.blade.php */ ?>
<?php $__env->startSection('content'); ?>
<div class="container" >
    <div class="row " >
        <div class="col-md-12">
            <div class="card" >
                <div class="card-header">
                  <div class="links">
                    <?php $__currentLoopData = $sections; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $section): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                       <a href="<?php echo e(url('section/')); ?>/<?php echo e($section->id); ?>"> <?php echo e($section->section_title); ?> </a> 
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </div>
                </div>                 
                <div class="card-body">
                    <?php if(isset($articles)): ?>
                    <?php $__currentLoopData = $articles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                       <div class="row" >
                            <div class="col-md-2" > <img src="<?php echo e(url($article->article_thumb)); ?>" width="60%"/></div>
                            <div class="col-md-3" ><a href="<?php echo e(url('article')); ?>/<?php echo e($article->article_id); ?>"><?php echo e($article->article_title); ?></a></div>                                                
                       </div>
                       <hr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>     
                    
                    <?php endif; ?>                
                                  
                </div>                 
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>