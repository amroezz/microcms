<?php /* /home/amr/Desktop/projects/microcms/microcms/resources/views/home.blade.php */ ?>
<script  src="<?php echo e(url('js/ck/ckeditor.js')); ?>"></script>
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<script  src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script  src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    <?php if(session('status')): ?>
                        <div class="alert alert-success" role="alert">
                            <?php echo e(session('status')); ?>

                        </div>
                    <?php endif; ?>

                    <div class="row">
                         <div class="col-lg-4 col-md-6">
                             <div class="card " style="background-color:#968cec;">
                                 <div class="card-body">
                                     <div class="d-flex flex-row">
                                         <div class="align-self-center m-l-20">
                                             <h3 class="text-white m-b-0"><?php echo e($stastics['articles']); ?></h3>
                                             <h2 class="text-white m-b-0">Articles</h2>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                         <div class="col-lg-4 col-md-6">
                             <div class="card" style="background-color:#1976d2;">
                                 <div class="card-body">
                                     <div class="d-flex flex-row">
                                         <div class="align-self-center m-l-20">
                                             <h3 class="text-white m-b-0 "><?php echo e($stastics['featured_articles']); ?></h3>
                                             <h2 class="text-white m-b-0">featured articles</h2>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                    </div>

                    <br><br>
                    
                    <div class="row">
                        <div class="col-md-12">
                        <a class="btn btn-info" href="<?php echo e(url('articles/create')); ?>"> <i class="mdi mdi-library-plus"></i>  Add New Article</a>
                        </div>
                    </div>
                    
                    <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Article Title</th>
                <th>Article Type</th>
                <th>Article Section</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            
              <?php $__currentLoopData = $articles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                  <td><?php echo e($article->article_title); ?></td>
                  <td><?php if($article->article_type == 1): ?><p>Article</p><?php else: ?> <p>Featured Article</p> <?php endif; ?></td>
                  <td><?php echo e($article->section_id); ?></td>
                  <td>

                  <a href="<?php echo e(route('articles.edit',$article->article_id)); ?>" class="btn waves-effect waves-light btn-outline-info" data-toggle="tooltip" data-placement="top" title="" >Edit</a>

                   
                   <form action="<?php echo e(url('articles', [$article->article_id])); ?>" method="POST" >
                   <input type="hidden" name="_method" value="DELETE">
                   <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                   <button class="btn btn-danger">Delete</button>
                   
                   </form>
                  </td>
                </tr>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            
        </tbody>
            
            
    </table>

    
                    
                    <!-- <div class="form-group">
                                    <lable>الموضـــوع</lable>
                                    <textarea name="details" id="editor1" rows="10" cols="80"></textarea>
                                    <?php if($errors->has('details')): ?> <div class="text-danger"><?php echo e($errors->first('details')); ?></div>     <?php endif; ?>
                                    <script src="//cdn.ckeditor.com/4.11.1/basic/ckeditor.js"></script>

                                    <script type="text/javascript">
                                        CKEDITOR.replace( 'editor1', {
                                        });
                                        
                                    </script>
        
                                </div>
                </div> -->
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>