<?php /* /home/amr/Desktop/projects/microcms/microcms/resources/views/articles_edit.blade.php */ ?>
<script  src="<?php echo e(url('js/ck/ckeditor.js')); ?>"></script>
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<script  src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script  src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">New Article</div>

                <div class="card-body">
                    <?php if(session('status')): ?>
                        <div class="alert alert-success" role="alert">
                            <?php echo e(session('status')); ?>

                        </div>
                    <?php endif; ?>

                   

    
                    <form action="<?php echo e(route('articles.update', $article->article_id)); ?>" method="post" enctype="multipart/form-data">
                     <?php echo e(csrf_field()); ?>

                     <?php echo e(method_field('PUT')); ?>

                    <div class="form-group">
                       <lable>Title</lable>
                       <input type="text" name="article_title" id="article_title" class="form-control" value="<?php echo e($article->article_title); ?>">
                       <?php if($errors->has('article_title')): ?> <div class="text-danger"><?php echo e($errors->first('article_title')); ?></div> <?php endif; ?>
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                           <div class="form-group">
                              <lable>Article Type</lable>
                              <select class="form-control" name="article_type" id="article_type" > 
                                 <option value="1" <?php echo e(isset($article->article_type)==1 ? "selected" : ""); ?>>Article</option>
                                 <option value="2" <?php echo e(isset($article->article_type)==2 ? "selected" : ""); ?>>Featured Article</option>
                              </select>
                              <?php if($errors->has('article_type')): ?> <div class="text-danger"><?php echo e($errors->first('article_type')); ?></div> <?php endif; ?>
                           </div>
                      </div>
                      <div class="col-md-4">
                           <div class="form-group">
                              <lable>Article Section</lable>
                              <select class="form-control" name="section_id" id="section_id">
                                 <?php $__currentLoopData = $sections; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $section): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                     <option value="<?php echo e($section->id); ?>" <?php echo e(isset($article->section_id)==$section->id ? "selected" : ""); ?>><?php echo e($section->section_title); ?></option>
                                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>                                 
                              </select>
                              <?php if($errors->has('section_id')): ?> <div class="text-danger"><?php echo e($errors->first('section_id')); ?></div> <?php endif; ?>
                           </div>
                      </div>
                      <div class="col-md-4">
                           <div class="form-group">
                              <lable>Article Order</lable>
                              <input type="number" min="1" name="article_order" id="article_order" class="form-control" value="<?php echo e($article->article_order); ?>">
                              <?php if($errors->has('article_order')): ?> <div class="text-danger"><?php echo e($errors->first('article_order')); ?></div> <?php endif; ?>
                           </div>
                      </div>
                    </div>
                        <div class="form-group">
                              <lable>Article Image</lable>
                              <input type="file" name="article_thumb" id="article_thumb" class="form-control" >
                              <img src="<?php echo e(url($article->article_thumb)); ?>" />
                              <?php if($errors->has('article_thumb')): ?> <div class="text-danger"><?php echo e($errors->first('article_thumb')); ?></div> <?php endif; ?>
                        </div>
                    <div class="form-group">
                                    <lable>Details</lable>
                                    <textarea name="article_details" id="editor1" rows="10" cols="80"><?php echo e($article->article_details); ?></textarea>
                                    <?php if($errors->has('article_details')): ?> <div class="text-danger"><?php echo e($errors->first('article_details')); ?></div>     <?php endif; ?>
                                    <script src="//cdn.ckeditor.com/4.11.1/basic/ckeditor.js"></script>

                                    <script type="text/javascript">
                                        CKEDITOR.replace( 'editor1', {
                                        });
                                        
                                    </script>
        
                                </div>

                                <div class="form-group">
                                 <button type="submit" class="btn btn-success">Update Article</button>
                                </div>
                                </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>