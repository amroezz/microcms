@extends('layouts.app')
@section('content')
<div class="container" >
    <div class="row " >
        <div class="col-md-12">
            <div class="card" >
                <div class="card-header">
                  <div class="links">
                    @foreach($sections as $section)
                       <a href="{{url('section/')}}/{{$section->id}}"> {{$section->section_title}} </a> 
                    @endforeach
                  </div>
                </div>                 
                <div class="card-body">
                    @if(isset($articles))
                    @foreach($articles as $article)
                       <div class="row" >
                            <div class="col-md-2" > <img src="{{url($article->article_thumb)}}" width="60%"/></div>
                            <div class="col-md-3" ><a href="{{url('article')}}/{{$article->article_id}}">{{$article->article_title}}</a></div>                                                
                       </div>
                       <hr>
                    @endforeach     
                    
                    @endif               1 
                                  
                </div>                 
            </div>
        </div>
    </div>
</div>
@endsection

