@extends('layouts.app')
<script  src="{{url('js/ck/ckeditor.js')}}"></script>
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
<script  src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script  src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">New Article</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                   

    
                    <form action="{{ route('articles.update', $article->article_id) }}" method="post" enctype="multipart/form-data">
                     {{ csrf_field() }}
                     {{ method_field('PUT') }}
                    <div class="form-group">
                       <lable>Title</lable>
                       <input type="text" name="article_title" id="article_title" class="form-control" value="{{$article->article_title }}">
                       @if ($errors->has('article_title')) <div class="text-danger">{{ $errors->first('article_title') }}</div> @endif
                    </div>
                    <div class="row">
                      <div class="col-md-4">
                           <div class="form-group">
                              <lable>Article Type</lable>
                              <select class="form-control" name="article_type" id="article_type" > 
                                 <option value="1" {{isset($article->article_type)==2 ? "selected" : "" }}>Article</option>
                                 <option value="2" {{isset($article->article_type)==1 ? "selected" : "" }}>Featured Article</option>
                              </select>
                              @if ($errors->has('article_type')) <div class="text-danger">{{ $errors->first('article_type') }}</div> @endif
                           </div>
                      </div>
                      <div class="col-md-4">
                           <div class="form-group">
                              <lable>Article Section</lable>
                              <select class="form-control" name="section_id" id="section_id">
                                 @foreach($sections as $section)
                                     <option value="{{$section->id}}" {{isset($article->section_id)==$section->id ? "selected" : "" }}>{{$section->section_title}}</option>
                                 @endforeach                                 
                              </select>
                              @if ($errors->has('section_id')) <div class="text-danger">{{ $errors->first('section_id') }}</div> @endif
                           </div>
                      </div>
                      <div class="col-md-4">
                           <div class="form-group">
                              <lable>Article Order</lable>
                              <input type="number" min="1" name="article_order" id="article_order" class="form-control" value="{{ $article->article_order }}">
                              @if ($errors->has('article_order')) <div class="text-danger">{{ $errors->first('article_order') }}</div> @endif
                           </div>
                      </div>
                    </div>
                        <div class="form-group">
                              <lable>Article Image</lable>
                              <input type="file" name="article_thumb" id="article_thumb" class="form-control" >
                              <img src="{{url($article->article_thumb)}}" />
                              @if ($errors->has('article_thumb')) <div class="text-danger">{{ $errors->first('article_thumb') }}</div> @endif
                        </div>
                    <div class="form-group">
                                    <lable>Details</lable>
                                    <textarea name="article_details" id="editor1" rows="10" cols="80">{{ $article->article_details }}</textarea>
                                    @if ($errors->has('article_details')) <div class="text-danger">{{ $errors->first('article_details') }}</div>     @endif
                                    <script src="//cdn.ckeditor.com/4.11.1/basic/ckeditor.js"></script>

                                    <script type="text/javascript">
                                        CKEDITOR.replace( 'editor1', {
                                        });
                                        
                                    </script>
        
                                </div>

                                <div class="form-group">
                                 <button type="submit" class="btn btn-success">Update Article</button>
                                </div>
                                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
