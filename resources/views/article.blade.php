@extends('layouts.app')
@section('content')
<div class="container" >
    <div class="row " >
        <div class="col-md-12">
            <div class="card" >
                <div class="card-header">
                  <div class="links">
                    @foreach($sections as $section)
                       <a href="{{url('section')}}/{{$section->id}}"> {{$section->section_title}} </a> 
                    @endforeach
                  </div>
                </div>                 
                <div class="card-body" >                
                       <div class="row" >
                       <img src="{{url($article->article_thumb)}}" >
                       </div>
                       <div class="row" >
                       {!! $article->article_details !!}
                       </div>
                </div>                 
            </div>
        </div>
    </div>
</div>
@endsection

