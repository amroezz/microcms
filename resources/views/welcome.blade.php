@extends('layouts.app')
@section('content')
<div class="container" >
    <div class="row " >
        <div class="col-md-12">
            <div class="card" >
                <div class="card-header">
                  <div class="links">
                    @foreach($sections as $section)
                       <a href="{{url('section/').$section->id}}"> {{$section->section_title}} </a> 
                    @endforeach
                  </div>
                </div>                 
                <div class="card-body"> 
                    <h3>Article</h3>               
                    @foreach($articles as $article)
                       <div class="row" >
                            <div class="col-md-2" > <img src="{{url($article->article_thumb)}}" width="60%"/></div>
                            <div class="col-md-3" ><a href="{{url('article')}}/{{$article->article_id}}">{{$article->article_title}}</a></div>                                                
                       </div>
                       <hr>
                    @endforeach  


                    <h3>Featured Article</h3>               
                    @foreach($featured_articles as $article)
                       <div class="row" >
                            <div class="col-md-2" > <img src="{{url($article->article_thumb)}}" width="60%"/></div>
                            <div class="col-md-3" ><a href="{{url('article')}}/{{$article->article_id}}">{{$article->article_title}}</a></div>                                                
                       </div>
                       <hr>
                    @endforeach                   
                </div>                 
            </div>
        </div>
    </div>
</div>
@endsection

