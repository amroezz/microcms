<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\articles;
use App\sections;

class HomeController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $articles          = articles::where('article_type',2)->count();
        $featured_articles = articles::where('article_type',1)->count();
        $stastics = array(
            'articles' => $articles,
            'featured_articles' => $featured_articles
        );

        $articles = articles::All();
        return view('home')->with('stastics', $stastics)->with('articles', $articles);
    }

    public function welcome()
    {       
        $articles = articles::where('article_type',2)->get();
        $featured_articles = articles::where('article_type',1)->get();
        $sections = sections::All();
        return view('welcome')->with('sections', $sections)->with('articles', $articles)
        ->with('featured_articles', $featured_articles);
    }

    public function section($id)
    {       
        $articles = articles::where('section_id',$id)->get();
        $sections = sections::All();
        return view('section')->with('sections', $sections)->with('articles', $articles);
    }
    
    public function article($id)
    {       
        $article = articles::where('article_id',$id)->get()->first();  
        $sections = sections::All();      
        return view('article')->with('sections', $sections)->with('article', $article);
    }
}
