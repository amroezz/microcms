<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\articles;
use App\sections;
use Validator;
use DB;

class articlesController extends Controller
{
    
    protected $messages = [
        'article_title.required'   => 'title is required',       
        'article_type.required'    => 'article type is required',   
        'section_id.required'    => 'Section type is required',   
        'article_details.required' => 'details is required',   
        
    ];

    public function index()
    {
        $articles = articles::All();
        return view('home')->with('articles',$articles);
    }

    
    public function create()
    {
        $sections = sections::All();
        return view('articles_add')->with('sections',$sections);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'article_title'   => 'required',
            'article_type'    => 'required',
            'section_id'      => 'required',
            'article_details' => 'required'
        ], $this->messages);

        if ($validator->fails()) {
            return redirect('articles/create')
                        ->withErrors($validator)
                        ->withInput();
        } 
        else {
            # Store Data
            $article = new articles();
            $article->article_title   = $request->article_title;
            $article->article_details = $request->article_details;
            $article->article_type    = $request->article_type;
            $article->article_order   = isset($request->article_order) ? $request->article_order : 1;
            $article->section_id      = $request->section_id;
            if(isset($request->article_thumb))
            {
                $path = $this->upload_image($request->article_thumb);
                $article->article_thumb   = $path;
            }                
            $article->save();
        }
        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sections = sections::All();
        $article = articles::where('article_id',$id)->get()->first();
        return view('articles_edit')->with('article',$article)->with('sections',$sections);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'article_title'   => 'required',
            'article_type'    => 'required',
            'section_id'      => 'required',
            'article_details' => 'required'
        ], $this->messages);

        if ($validator->fails()) {
            return redirect('articles/create')
                        ->withErrors($validator)
                        ->withInput();
        } 
        else {
            # Store Data
                       
            if(isset($request->article_thumb))
            {
                $path = $this->upload_image($request->article_thumb);
                DB::table('articles')->where('article_id', '=', $id )
                 ->update([
                    'article_title'       => $request->article_title,
                    'article_details'         => $request->article_details,
                    'article_type'               => $request->article_type,
                    'article_order'               => $request->article_order,
                    'section_id'               => $request->section_id,
                    'article_thumb'               => $request->path,
                ]);  
            } 
            else{
                DB::table('articles')->where('article_id', '=', $id )
                 ->update([
                    'article_title'       => $request->article_title,
                    'article_details'         => $request->article_details,
                    'article_type'               => $request->article_type,
                    'article_order'               => $request->article_order,
                    'section_id'               => $request->section_id,
            ]);  
            }               
        }
        return redirect()->route('home');
    }

    public function destroy($id)
    {
        $article = articles::where('article_id',$id)->get()->first();
        Db::table('articles')->where('article_id', '=', $id)->delete(); 
        return redirect()->route('home');
    }

    public function upload_image($image)
    {
        $image_name = time().'.'.$image->getClientOriginalExtension();

        $destinationPath = public_path('/images/articles/');
        $image->move($destinationPath, $image_name);
        return '/images/articles/'.$image_name;
    }
}
